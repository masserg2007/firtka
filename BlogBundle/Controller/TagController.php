<?php

namespace BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use A2lix\I18nDoctrineBundle\Annotation\I18nDoctrine;

use BlogBundle\Entity\Tags;
use BlogBundle\Form\Type\TagType;

use MenuBundle\Entity\Routes;

class TagController extends Controller
{
    public function tagsAction()
    {
        $tags = $this->getDoctrine()->getRepository('BlogBundle:Tags')->findAll();
        return $this->render('BlogBundle:Default:tag_view.html.twig', ['tags' => $tags]);
    }

    public function tagCreateAction(Request $request)
    {
        $form = $this->createForm(TagType::class, new Tags());
        $form->handleRequest($request);
        if ($request->getMethod() == 'POST')
        {
            if ($form->isValid())
            {
                $data = $form->getData();
                if($data->getPid() != NULL) {
                    $data->setParent($this->getDoctrine()->getRepository('BlogBundle:Tags')->find($data->getPid()));
                }
                $em = $this->getDoctrine()->getManager();
                $em->persist($data);
                $em->flush();

                // SET ROUTE FOR MAIN MENU
                $route_param_slug = array();
                foreach($data->getTranslations() as $item){
                    $route_param_slug[$item->getLocale()] = $item->getSlug();
                }
                $route = new Routes();
                $route->setModule('blog');
                $route->setRoute('blog_tag_posts');
                $route->setParamId($data->getId());
                $route->setParamSlug(json_encode($route_param_slug));
                $em = $this->getDoctrine()->getManager();
                $em->persist($route);
                $em->flush();
                // END ROUTE

                return $this->redirect($this->generateUrl('blog_tags'));
            }
        }
        return $this->render('BlogBundle:Default:tag_create.html.twig', array('form' => $form->createView()));
    }

    /**
     * Edit object
     *
     * @I18nDoctrine
     */
    public function tagEditAction($id, Request $request)
    {
        $tag = $this->getDoctrine()->getRepository('BlogBundle:Tags')->find($id);
        $form = $this->createForm(TagType::class, $tag);
        $form->handleRequest($request);
        if ($request->getMethod() == 'POST')
        {
            if ($form->isValid())
            {
                $data = $form->getData();
                $em = $this->getDoctrine()->getManager();
                $em->persist($data);
                $em->flush();

                // SET ROUTE FOR MAIN MENU
                $route_param_slug = array();
                foreach($data->getTranslations() as $item){
                    $route_param_slug[$item->getLocale()] = $item->getSlug();
                }
                $route = $this->getDoctrine()->getRepository('MenuBundle:Routes')->findBy(
                    [
                        'module' => 'blog',
                        'param_id' => $id,
                        'route' => 'blog_tag_posts'
                    ]);
                if($route == null){
                    $route = new Routes();
                    $route->setModule('blog');
                    $route->setRoute('blog_tag_posts');
                    $route->setParamId($data->getId());
                }
                $route->setParamSlug(json_encode($route_param_slug));
                $em = $this->getDoctrine()->getManager();
                $em->persist($route);
                $em->flush();
                // END ROUTE

                return $this->redirect($this->generateUrl('blog_tags'));
            }
        }
        return $this->render('BlogBundle:Default:tag_create.html.twig', array('form' => $form->createView()));
    }

    public function tagRemoveAction($id)
    {
        $tag = $this->getDoctrine()->getRepository('BlogBundle:Tags')->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($tag);
        $em->flush();

        return $this->redirect($this->generateUrl('blog_tags'));
    }
}
