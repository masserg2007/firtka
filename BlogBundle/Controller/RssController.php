<?php

namespace BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use A2lix\I18nDoctrineBundle\Annotation\I18nDoctrine;

use BlogBundle\Entity\Rss;
use BlogBundle\Form\Type\RssType;


class RssController extends Controller
{
    public function rssAction()
    {
        $rss = $this->getDoctrine()->getRepository('BlogBundle:Rss')->findAll();
        return $this->render('BlogBundle:Default:rss_view.html.twig', ['rss' => $rss]);
    }

    public function rssCreateAction(Request $request)
    {
        $form = $this->createForm(RssType::class, new Rss());
        $form->handleRequest($request);
        if ($request->getMethod() == 'POST')
        {
            if ($form->isValid())
            {
                $data = $form->getData();
                $em = $this->getDoctrine()->getManager();
                $em->persist($data);
                $em->flush();

                return $this->redirect($this->generateUrl('blog_rss'));
            }
        }
        return $this->render('BlogBundle:Default:rss_create.html.twig', array('form' => $form->createView()));
    }

    /**
     * Edit object
     *
     * @I18nDoctrine
     */
    public function rssEditAction($id, Request $request)
    {
        $rss = $this->getDoctrine()->getRepository('BlogBundle:Rss')->find($id);
        $form = $this->createForm(RssType::class, $rss);
        $form->handleRequest($request);
        if ($request->getMethod() == 'POST')
        {
            if ($form->isValid())
            {
                $data = $form->getData();
                $em = $this->getDoctrine()->getManager();
                $em->persist($data);
                $em->flush();

                return $this->redirect($this->generateUrl('blog_rss'));
            }
        }
        return $this->render('BlogBundle:Default:rss_create.html.twig', array('form' => $form->createView()));
    }

    public function rssRemoveAction($id)
    {
        $rss = $this->getDoctrine()->getRepository('BlogBundle:Rss')->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($rss);
        $em->flush();

        return $this->redirect($this->generateUrl('blog_rss'));
    }
}
