<?php

namespace BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use A2lix\I18nDoctrineBundle\Annotation\I18nDoctrine;

use BlogBundle\Entity\Priority;
use BlogBundle\Form\Type\PriorityType;


class PriorityController extends Controller
{
    public function prioritiesAction()
    {
        $priorities = $this->getDoctrine()->getRepository('BlogBundle:Priority')->findAll();
        return $this->render('BlogBundle:Default:priority_view.html.twig', ['priorities' => $priorities]);
    }

    public function priorityCreateAction(Request $request)
    {
        $form = $this->createForm(PriorityType::class, new Priority());
        $form->handleRequest($request);
        if ($request->getMethod() == 'POST')
        {
            if ($form->isValid())
            {
                $data = $form->getData();
                $em = $this->getDoctrine()->getManager();
                $em->persist($data);
                $em->flush();

                return $this->redirect($this->generateUrl('blog_priority'));
            }
        }
        return $this->render('BlogBundle:Default:priority_create.html.twig', array('form' => $form->createView()));
    }

    /**
     * Edit object
     *
     * @I18nDoctrine
     */
    public function priorityEditAction($id, Request $request)
    {
        $priority = $this->getDoctrine()->getRepository('BlogBundle:Priority')->find($id);
        $form = $this->createForm(PriorityType::class, $priority);
        $form->handleRequest($request);
        if ($request->getMethod() == 'POST')
        {
            if ($form->isValid())
            {
                $data = $form->getData();
                $em = $this->getDoctrine()->getManager();
                $em->persist($data);
                $em->flush();

                return $this->redirect($this->generateUrl('blog_priority'));
            }
        }
        return $this->render('BlogBundle:Default:priority_create.html.twig', array('form' => $form->createView()));
    }

    public function priorityRemoveAction($id)
    {
        $priority = $this->getDoctrine()->getRepository('BlogBundle:Priority')->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($priority);
        $em->flush();

        return $this->redirect($this->generateUrl('blog_priority'));
    }
}
