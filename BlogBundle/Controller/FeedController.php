<?php

namespace BlogBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use \FeedIo\Factory;
use \FeedIo\Feed;
use Symfony\Component\Validator\Constraints\DateTime;

class FeedController extends Controller
{
    public function indexFeedAction()
    {

        $rss = $this->getDoctrine()->getRepository('BlogBundle:Rss')->findAll();
        return $this->render('BlogBundle:Rss:index.html.twig', ['rss' => $rss]);
    }

    public function generatorFeedAction($slug, Request $request)
    {
        $uri = $request->server->get('HTTP_HOST');
        $rss = $this->getDoctrine()->getRepository('BlogBundle:RssTranslation')->findOneBySlug($slug);
        $posts = $this->getDoctrine()->getRepository('BlogBundle:Posts')->findBy(['rss_id'=>$rss]);

        $feed = new Feed();
//        $feed->setLink('/blog/view/');
//        $feed->setUrl($uri);
//        $feed->set('version','2.0');
        $feed->setTitle($rss->getTitle());
        $feed->setLastModified(new \DateTime("now"));

        foreach ($posts as $p) {
            $item = $feed->newItem();
            $item->set('author',$p->getAuthor());
            $item->setTitle($p->getTitle());
            $item->setLastModified($p->getCreation());
            $item->setLink(':8093/blog/view/'.$p->getSlug());
            $item->setDescription($p->getDescription());
            $feed->add($item);
        }

        $feedIo = Factory::create()->getFeedIo();

        return new Response($feedIo->format($feed, 'atom'));
    }
}
