<?php

namespace BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

use A2lix\I18nDoctrineBundle\Annotation\I18nDoctrine;

use BlogBundle\Entity\Posts;
use BlogBundle\Form\Type\PostType;
use BlogBundle\Entity\PostImages;
use BlogBundle\Entity\Comments;
use BlogBundle\Form\Type\CommentType;
use BlogBundle\Entity\PostVotes;

use MenuBundle\Entity\Routes;

class PostController extends Controller
{
    public function getAjaxPostsAction(Request $request, $page, $perpage)
    {
        $em = $this->getDoctrine()->getManager();
        $date = date('Y-m-d H:i');
        $query = $em->createQuery("SELECT p FROM BlogBundle:Posts p WHERE p.begin_date<='$date' AND p.tag=2 ORDER BY p.id DESC");
        $paginator  = $this->get('knp_paginator');
        $posts = $paginator->paginate(
            $query,
            $page,
            $request->query->getInt('limit', $perpage)
        );

        return $this->render('BlogBundle:Default:post_index_view.html.twig',
            [
                'posts' => $posts,
                'page' => $page,
                'perpage' => $perpage
            ]);
    }

    public function postsAction(Request $request, $page, $perpage)
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery("SELECT p FROM BlogBundle:Posts p ORDER BY p.id DESC");
        $paginator  = $this->get('knp_paginator');
        $posts = $paginator->paginate(
            $query,
            $page,
            $request->query->getInt('limit', $perpage)
        );

        return $this->render('BlogBundle:Default:post_view.html.twig',
            [
                'posts' => $posts,
                'page' => $page,
                'perpage' => $perpage
            ]);
    }

    public function postCreateAction(Request $request)
    {
        $form = $this->createForm(PostType::class, new Posts());
        $form->handleRequest($request);
        if ($request->getMethod() == 'POST')
        {
            if ($form->isValid())
            {
                $data = $form->getData();
                $data->setUsers($user = $this->get('security.token_storage')->getToken()->getUser());
                $data->setTag($this->getDoctrine()->getRepository('BlogBundle:Tags')->find($data->getTagId()));
                $data->setCreation(new \DateTime("now"));

                $em = $this->getDoctrine()->getManager();
                $em->persist($data);
                $em->flush();

                $post_id = $data->getId();

                if(!is_dir("data/blog")){mkdir("data/blog", 0755);}
                if(!is_dir("data/blog/".$data->getId())){mkdir("data/blog/".$post_id, 0755);}

                // IMAGES
                foreach($data->getFiles() as $file) {
                    $fileTitle = $file->getClientOriginalName();
                    $fileMimeType = $file->getMimeType();
                    $fileSize = $file->getClientSize();
                    $fileExtension = $file->guessExtension();
                    $fileName = md5(uniqid()).'.'.$fileExtension;
                    $file->move(__DIR__.'/../../../web/data/blog/'.$post_id.'/',$fileName);

                    $image = new PostImages();
                    $image->setPost($data);
                    $image->setTitle($fileTitle);
                    $image->setPath($fileName);
                    $image->setImgType($fileMimeType);
                    $image->setImgSize($fileSize);
                    $image->setCreation(new \DateTime("now"));

                    $em = $this->getDoctrine()->getManager();
                    $em->persist($image);
                    $em->flush();
                }
                // END IMAGES

                // SET ROUTE FOR MAIN MENU
                $route_param_slug = array();
                foreach($data->getTranslations() as $item){
                    $route_param_slug[$item->getLocale()] = $item->getSlug();
                }
                $route = new Routes();
                $route->setModule('blog');
                $route->setRoute('blog_view_one');
                $route->setParamId($data->getId());
                $route->setParamSlug(json_encode($route_param_slug));
                $em = $this->getDoctrine()->getManager();
                $em->persist($route);
                $em->flush();
                // END ROUTE

                return $this->redirect($this->generateUrl('blog_posts'));
            }
        }
        return $this->render('BlogBundle:Default:post_create.html.twig', array('form' => $form->createView()));
    }

    /**
     * Edit object
     *
     * @I18nDoctrine
     */
    public function postEditAction($id, Request $request)
    {
        $post = $this->getDoctrine()->getRepository('BlogBundle:Posts')->find($id);

        $images = $post->getImages();
        $imagesArray = [];
        foreach($images as $i){
            $title = $i->getTitle();
            $path = $i->getPath();
            $type = $i->getImgType();
            $size = $i->getImgSize();

            $imagesArray[] = array(
                "name" => $path,
                "type" =>  $type,
                "size" => $size,
                "file" => '/data/blog/'.$id.'/'.$path,
                "data" => array("url" => '/data/blog/'.$id.'/'.$path)
            );
        }
        $imagesArray = json_encode($imagesArray);

        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);
        if ($request->getMethod() == 'POST')
        {
            if ($form->isValid())
            {
                $data = $form->getData();
//                $data->setUsers($user = $this->get('security.token_storage')->getToken()->getUser());
                $data->setUsers($data->getUsers());
                $data->setTag($this->getDoctrine()->getRepository('BlogBundle:Tags')->find($data->getTagId()));
                $data->setCreation(new \DateTime("now"));
                $em = $this->getDoctrine()->getManager();
                $em->persist($data);
                $em->flush();

                // SET ROUTE FOR MAIN MENU
                $route_param_slug = array();
                foreach($data->getTranslations() as $item){
                    $route_param_slug[$item->getLocale()] = $item->getSlug();
                }
                $route = $this->getDoctrine()->getRepository('MenuBundle:Routes')->findBy(
                    [
                        'module' => 'blog',
                        'param_id' => $id
                    ]);
                if($route != NULL){
                    $route = $route[0];
                } else {
                    $route = new Routes();
                    $route->setModule('post');
                    $route->setRoute('post_view_one');
                    $route->setParamId($data->getId());
                }
                $route->setParamSlug(json_encode($route_param_slug));
                $em = $this->getDoctrine()->getManager();
                $em->persist($route);
                $em->flush();
                // END ROUTE

                return $this->redirect($this->generateUrl('blog_posts'));
            }
        }
        return $this->render('BlogBundle:Default:post_create.html.twig', array(
            'form' => $form->createView(),
            'imagesArray' => $imagesArray,
            'post_id' => $id
        ));
    }

    public function postCommentAnswerAction(Request $request, $id)
    {
        $comment = $this->getDoctrine()->getRepository('BlogBundle:Comments')->find($id);
        $slug = $comment->getPost()->getSlug();
        $answer = new Comments();
        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')){
            $user = $this->get('security.token_storage')->getToken()->getUser();
            if($user->getProfile() != NULL){
                $profile = $user->getProfile();
                $firstname = $profile->getFirstName();
                $lastname = $profile->getLastName();
                if($firstname != NULL || $lastname != NULL ){
                    $name = $lastname.' '.$firstname;
                } else {
                    $name = $user->getUsername();
                }
            } else {
                $name = $user->getUsername();
            }
            $answer->setName($name);
            $answer->setEmail($user->getEmail());
        } else {
            $user = NULL;
        }

        $formAnswer = $this->createForm(CommentType::class, $answer);
        $formAnswer->handleRequest($request);

        if ($request->getMethod() == 'POST')
        {
            if ($formAnswer->isValid())
            {
                $data = $formAnswer->getData();
                $data->setUser($user);
                $data->setPost($comment->getPost());
                $data->setParent($comment);
                $data->setCreation(new \DateTime("now"));

                $em = $this->getDoctrine()->getManager();
                $em->persist($data);
                $em->flush();

                return $this->redirect($this->generateUrl('blog_view_one', array('slug'=>$slug)));
            }
        }
        return $this->render('BlogBundle:Default:comment_answer.html.twig', array(
            'comment' => $comment,
            'formAnswer' => $formAnswer->createView(),
        ));

    }

    public function postRemoveAction($id)
    {
        $post = $this->getDoctrine()->getRepository('BlogBundle:Posts')->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($post);
        $em->flush();

        $directory =  __DIR__.'/../../../web/data/blog/'.$id;

        if(is_dir("$directory")){
            array_map('unlink', glob("$directory/*.*"));
            rmdir($directory);
        }

        return $this->redirect($this->generateUrl('blog_posts'));
    }

    public function postPublicationAction($id)
    {
        $post = $this->getDoctrine()->getRepository('BlogBundle:Posts')->find($id);
        $post->setPublicated(1);
        $em = $this->getDoctrine()->getManager();
        $em->persist($post);
        $em->flush();

        return $this->redirect($this->generateUrl('admin'));
    }

    public function postAddImageAction($id)
    {
        $original_name = $_FILES['post']['name']['files'][0];
        $tmp_name = $_FILES['post']['tmp_name']['files'][0];
        $type = $_FILES['post']['type']['files'][0];
        $size = $_FILES['post']['size']['files'][0];
        $new_name = md5(uniqid()).'.'.strtr($type,['image/' => '']);

        $file = __DIR__.'/../../../web/data/blog/'.$id.'/'.$new_name;
        move_uploaded_file($tmp_name, $file);
        $imagesArray[] = array(
            "name" => $new_name,
            "type" => $type,
            "size" => $size,
            "file" => $file,
            "data" => array("url" => '/data/blog/'.$id.'/'.$new_name)
        );
        $imagesArray = json_encode($imagesArray);

        $post = $this->getDoctrine()->getRepository('BlogBundle:Posts')->find($id);
        $newImage = new PostImages();
        $newImage->setTitle($original_name);
        $newImage->setPath($new_name);
        $newImage->setImgType($type);
        $newImage->setImgSize($size);
        $newImage->setCreation(new \DateTime("now"));
        $post->addImage($newImage);

        $em = $this->getDoctrine()->getManager();
        $em->persist($post);
        $em->flush();

        return new Response($imagesArray);
    }

    public function postRemoveImageAction($id)
    {
        $path = $_POST['file'];
        $image = $this->getDoctrine()->getRepository('BlogBundle:PostImages')->findBy(['post_id' => $id, 'path' => $path])[0];
        if(is_file(__DIR__.'/../../../web/data/blog/'.$id.'/'.$image->getPath())){
            unlink(__DIR__.'/../../../web/data/blog/'.$id.'/'.$image->getPath());
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($image);
        $em->flush();

        return new Response('200');
    }

    public function postCommentRemoveAction($id)
    {
        $comment = $this->getDoctrine()->getRepository('BlogBundle:Comments')->find($id);
        $slug = $comment->getPost()->getSlug();
        $em = $this->getDoctrine()->getManager();
        $em->remove($comment);
        $em->flush();

        return $this->redirect($this->generateUrl('blog_view_one', ['slug' => $slug]));
    }

    public function postVoteAction(Request $request)
    {
        if ($request->isXmlHttpRequest()) {
            $array = json_decode($request->getContent(),true);
            if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
                $user_id = $this->get('security.token_storage')->getToken()->getUser()->getId();
            } else {
                $user_id = NULL;
            }
            $user_ip = $request->getClientIp();

            $post_id = $array['id'];
            $vote = $array['vote'];

            $cnt = $this->getDoctrine()->getRepository('BlogBundle:PostVotes')->getCountVotes($post_id,$user_id,$user_ip);
            if($cnt == 0) {
                $postVote = new PostVotes();
                $postVote->setUserId($user_id);
                $postVote->setPostId($post_id);
                $postVote->setVote($vote);
                $postVote->setUserIp($user_ip);
                $postVote->setCreation(new \DateTime("now"));

                $em = $this->getDoctrine()->getManager();
                $em->persist($postVote);
                $em->flush();
            }
        }
        $like = $this->getDoctrine()->getRepository('BlogBundle:PostVotes')->findBy(['post_id' => $post_id, 'vote' => 1]);
        $dislike = $this->getDoctrine()->getRepository('BlogBundle:PostVotes')->findBy(['post_id' => $post_id, 'vote' => -1]);
        return new Response(count($like).'::'.count($dislike));
    }

    public function postsVideosAllAction(Request $request, $page, $perpage)
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery("SELECT p FROM BlogBundle:Posts p WHERE p.tag_id = 8 ORDER BY p.creation DESC");
        $paginator  = $this->get('knp_paginator');
        $videos = $paginator->paginate(
            $query,
            $page,
            $request->query->getInt('limit', $perpage)
        );

        return $this->render('BlogBundle:Default:post_videos_all.html.twig',
            [
                'videos' => $videos,
                'page' => $page,
                'perpage' => $perpage
            ]);
    }

    public function searchSimilarPostsAction(Request $request){
        $title = $request->query->get('q');
        $posts = $this->getDoctrine()->getRepository('BlogBundle:Posts')->getSimilarBlogPosts($title);
        $result = [];
        foreach($posts as $p){
            $id = $p->getTranslatable()->getId();
            $title = $p->getTitle();
            array_push($result, "{ \"id\": $id, \"text\": \"$title\" }");
        }
        $result = "[".implode(",", $result)."]";
        $results = "{\"results\": ".$result."}";
        return new Response($results);
    }

}
