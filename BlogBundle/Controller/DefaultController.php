<?php

namespace BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Collections\Criteria;

use BlogBundle\Entity\PostViews;
use BlogBundle\Entity\Comments;
use BlogBundle\Form\Type\CommentType;
use BlogBundle\Form\Type\SearchType;


class DefaultController extends Controller
{
    public function indexAction(Request $request, $page, $perpage)
    {
        $tags = $this->getDoctrine()->getRepository('BlogBundle:Tags')->findAll();

        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery("SELECT p FROM BlogBundle:Posts p");
        $paginator  = $this->get('knp_paginator');
        $posts = $paginator->paginate(
            $query,
            $page,
            $request->query->getInt('limit', $perpage)
        );

        $form = $this->createForm(SearchType::class, NULL);
        $form->handleRequest($request);
        if ($request->getMethod() == 'POST')
        {
            if ($form->isValid())
            {
                $word = $form['search']->getData();
                $finder = $this->container->get('fos_elastica.finder.blog.post');
                $paginator = $this->get('knp_paginator');
                $results = $finder->createPaginatorAdapter('*'.$word.'*');
                $posts = $paginator->paginate($results, $page, $perpage);
            }
        }
        return $this->render('BlogBundle:Default:index.html.twig',
            [
                'posts' => $posts,
                'tags' => $tags,
                'page' => $page,
                'perpage' => $perpage,
                'form' => $form->createView()
            ]);
    }

    public function indexByTagsAction(Request $request, $slug, $page, $perpage)
    {
        $tag = $this->getDoctrine()->getRepository('BlogBundle:TagsTranslation')->findBy(['slug'=>$slug]);

        if ($tag != null){
            $tag_id = $tag[0]->getTranslatable()->getId();
        }

        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery("SELECT p FROM BlogBundle:Posts p WHERE p.tag_id=$tag_id");
        $paginator  = $this->get('knp_paginator');
        $posts = $paginator->paginate(
            $query,
            $page,
            $request->query->getInt('limit', $perpage)
        );

        $form = $this->createForm(SearchType::class, NULL);
        $form->handleRequest($request);
        if ($request->getMethod() == 'POST')
        {
            if ($form->isValid())
            {
                $word = $form['search']->getData();
                $finder = $this->container->get('fos_elastica.finder.blog.post');
                $paginator = $this->get('knp_paginator');
                $results = $finder->createPaginatorAdapter('*'.$word.'*');
                $posts = $paginator->paginate($results, $page, $perpage);
            }
        }
        if ($tag_id == 9){
            return $this->render('BlogBundle:Default:index_blog.html.twig',
                [
                    'posts' => $posts,
                    'page' => $page,
                    'perpage' => $perpage,
                    'form' => $form->createView()
                ]);
        } else {
            return $this->render('BlogBundle:Default:index.html.twig',
                [
                    'posts' => $posts,
                    'page' => $page,
                    'perpage' => $perpage,
                    'form' => $form->createView()
                ]);
        }
    }

    public function viewOneAction($slug, Request $request)
    {
        $post = $this->getDoctrine()->getRepository('BlogBundle:PostsTranslation')->findOneBySlug($slug);
        if($post != null) {
            if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
                $user = $this->get('security.token_storage')->getToken()->getUser();
                $user_id = $user->getId();
            } else {
                $user = NULL;
                $user_id = NULL;
            }
            $post_id = $post->getTranslatable()->getId();
            $user_ip = $request->getClientIp();
            $cnt = $this->getDoctrine()
                ->getRepository('BlogBundle:PostViews')
                ->getCountViews($post_id, $user_id, $user_ip);
            if ($cnt == 0) {
                $postView = new PostViews();
                $postView->setUserId($user);
                $postView->setUserIp($user_ip);
                $postView->setCreation(new \DateTime("now"));
                $post->getTranslatable()->addView($postView);

                $em = $this->getDoctrine()->getManager();
                $em->persist($post);
                $em->flush();
            }
        }
        $votes = $this->getDoctrine()->getRepository('BlogBundle:PostVotes')->getVotes($post->getTranslatable()->getId());
        $comments = $post->getTranslatable()->getComments()->matching(Criteria::create()->where(Criteria::expr()->eq("pid",null)));
        $images = $post->getTranslatable()->getImages();
        $img_array = [];
        foreach($images as $img){
            array_push($img_array,$img->getPath());
        }
        $images = array_chunk($img_array,4);

        if($post){
            $seoPage = $this->container->get('sonata.seo.page');
            $seoPage
                ->setTitle($post->getSeoTitle())
                ->addMeta('name', 'keywords', $post->getSeoKeyword())
                ->addMeta('name', 'description', $post->getSeoDescription())
                ->addMeta('property', 'og:title', $post->getSeoTitle())
                ->addMeta('property', 'og:type', 'blog')
                ->addMeta('property', 'og:url',  $this->generateUrl('blog_view_one', array('slug' => $post->getSlug()),true))
                ->addMeta('property', 'og:description', $post->getSeoDescription())
            ;
        }

        $comment = new Comments();
        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')){
            $user = $this->get('security.token_storage')->getToken()->getUser();
            if($user->getProfile() != NULL){
                $profile = $user->getProfile();
                $firstname = $profile->getFirstName();
                $lastname = $profile->getLastName();
                if($firstname != NULL || $lastname != NULL ){
                    $name = $lastname.' '.$firstname;
                } else {
                    $name = $user->getUsername();
                }
            } else {
                $name = $user->getUsername();
            }
            $comment->setName($name);
            $comment->setEmail($user->getEmail());
        } else {
            $user = null;
        }

        $formComment = $this->createForm(CommentType::class, $comment);
        $formComment->handleRequest($request);

        if ($request->getMethod() == 'POST')
        {
            if ($formComment->isValid())
            {
                $data = $formComment->getData();

                $data->setUser($user);
                $data->setPost($post->getTranslatable());
                $data->setCreation(new \DateTime("now"));

                $em = $this->getDoctrine()->getManager();
                $em->persist($data);
                $em->flush();

                return $this->redirect($this->generateUrl('blog_view_one', array('slug'=>$slug)));
            }
        }

        return $this->render('BlogBundle:Default:view_one.html.twig', [
            'post' => $post,
            'images' => $images,
            'votes' => $votes,
            'comments' => $comments,
            'formComment' => $formComment->createView()
        ]);
    }

    public function printOneAction($slug)
    {
        $post = $this->getDoctrine()->getRepository('BlogBundle:PostsTranslation')->findOneBySlug($slug);

        return $this->render('BlogBundle:Default:print_one.html.twig', ['post' => $post]);
    }

    public function searchAction($search=null, Request $request)
    {
        $form = $this->createForm(SearchType::class, NULL, ['search' => $search]);
        $form->handleRequest($request);
        return $this->render('BlogBundle:Default:search.html.twig', ['form' => $form->createView()]);
    }

    public function searchResultAction($page, $perpage, Request $request)
    {
        dump($request->request);
        if ($request->getMethod() == 'POST'){
            $word = $request->request->get('search')['search'];
        } else {
            $word = null;
        }

        $finder = $this->container->get('fos_elastica.finder.blog.post');
        $paginator = $this->get('knp_paginator');
        $results = $finder->createPaginatorAdapter($word);
        $posts = $paginator->paginate($results, $page, $perpage);

        return $this->render('BlogBundle:Default:index_search.html.twig', ['word' => $word, 'posts' => $posts]);
    }

    public function searchSimilarAction(Request $request)
    {
        $q = $request->query->get('q');
        $results = $this->getDoctrine()->getRepository('BlogBundle:Posts')->findLikeTitle($q);

        return $this->render('similar.html.twig', ['results' => $results]);
    }

    public function getAuthorAction($id = null)
    {
        $author = $this->getDoctrine()->getRepository('AppBundle:Author')->find($id);

        return new Response($author->getName());
    }

}
