<?php

namespace BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use A2lix\I18nDoctrineBundle\Annotation\I18nDoctrine;

use BlogBundle\Entity\Comment;
use BlogBundle\Form\Type\CommentAccessType;


class CommentAccessController extends Controller
{
    public function CommentAccessAction()
    {
        $comment_access = $this->getDoctrine()->getRepository('BlogBundle:Comment')->findAll();
        return $this->render('BlogBundle:Default:comment_access_view.html.twig', ['comment_access' => $comment_access]);
    }

    public function CommentAccessCreateAction(Request $request)
    {
        $form = $this->createForm(CommentAccessType::class, new Comment());
        $form->handleRequest($request);
        if ($request->getMethod() == 'POST')
        {
            if ($form->isValid())
            {
                $data = $form->getData();
                $em = $this->getDoctrine()->getManager();
                $em->persist($data);
                $em->flush();

                return $this->redirect($this->generateUrl('blog_comment_access'));
            }
        }
        return $this->render('BlogBundle:Default:comment_access_create.html.twig', array('form' => $form->createView()));
    }

    /**
     * Edit object
     *
     * @I18nDoctrine
     */
    public function CommentAccessEditAction($id, Request $request)
    {
        $comment_access = $this->getDoctrine()->getRepository('BlogBundle:Comment')->find($id);
        $form = $this->createForm(CommentAccessType::class, $comment_access);
        $form->handleRequest($request);
        if ($request->getMethod() == 'POST')
        {
            if ($form->isValid())
            {
                $data = $form->getData();
                $em = $this->getDoctrine()->getManager();
                $em->persist($data);
                $em->flush();

                return $this->redirect($this->generateUrl('blog_comment_access'));
            }
        }
        return $this->render('BlogBundle:Default:comment_access_create.html.twig', array('form' => $form->createView()));
    }

    public function CommentAccessRemoveAction($id)
    {
        $comment_access = $this->getDoctrine()->getRepository('BlogBundle:Comment')->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($comment_access);
        $em->flush();

        return $this->redirect($this->generateUrl('blog_comment_access'));
    }
}
