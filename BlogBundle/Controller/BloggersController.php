<?php
namespace BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;



class BloggersController extends Controller
{
    public function indexAction()
    {
        $all_bloggers = $this->getDoctrine()->getRepository('BlogBundle:Posts')->getAllBloggers();

        return $this->render('BlogBundle:Bloggers:index.html.twig',
        [
            'all_bloggers' => $all_bloggers
        ]
        );
    }

    public function viewAction(Request $request, $id, $page, $perpage)
    {

        $all_post_blogger = $this->getDoctrine()->getRepository('BlogBundle:Posts')->getAllPostsAuthor($id);
        $paginator  = $this->get('knp_paginator');
        $posts = $paginator->paginate(
            $all_post_blogger,
            $page,
            $request->query->getInt('limit', $perpage)
        );
        $user = $all_post_blogger[0]->getUsers();
        return $this->render('BlogBundle:Bloggers:view.html.twig',
            [
                'user' => $user,
                'posts' => $posts,
                'page' => $page,
                'perpage' => $perpage,
            ]
        );
    }

    public function bloggerPostNewAllAction(Request $request, $page, $perpage)
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery("SELECT p FROM BlogBundle:Posts p WHERE p.tag_id=10 ORDER BY p.creation DESC");
        $paginator  = $this->get('knp_paginator');
        $posts = $paginator->paginate(
            $query,
            $page,
            $request->query->getInt('limit', $perpage)
        );

        return $this->render('BlogBundle:Bloggers:view_all_new_posts.html.twig',
            [
                'posts' => $posts,
                'page' => $page,
                'perpage' => $perpage
            ]);
    }
}