<?php

namespace BlogBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

use Doctrine\ORM\EntityManager;

use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

use A2lix\TranslationFormBundle\Form\Type\TranslationsType;

class TagType extends AbstractType
{
    protected $em;
    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('pid', ChoiceType::class, array(
            'required' => false,
            'placeholder' => 'main',
            'choices' => $this->par(),
        ));
        $builder->add('translations', TranslationsType::class, array(
                        'fields' => array(
                            'translatable_id' => array(
                                'field_type' => HiddenType::class,
                            ),
                            'title' => array(
                                'label' => 'Title',
                                'field_type' => TextType::class,
                                'required' => true
                            ),
                            'slug' => array(
                                'label' => 'Alias',
                                'field_type' => TextType::class,
                                'required' => false
                            ),
                        ),
                        'label' => false
                ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BlogBundle\Entity\Tags'
        ));
    }

    public function par()
    {
        $result = $this->em->getRepository("BlogBundle:Tags")->findBy(['pid' => NULL]);
        $return = array();
        foreach ($result as $value) {
            $return[$value->getTitle()] = $value->getId();

            if(count($value->getChildrens())>0){
                $separator = '-';
                $return = $this->chi($value->getId(), $return, $separator);
            }
        }
        return $return;
    }

    public function chi($id, $return, $separator)
    {
        $result = $this->em->getRepository("BlogBundle:Tags")->findBy(['pid' => $id]);
        foreach ($result as $value) {
            $return[$separator.$value->getTitle()] = $value->getId();
            if(count($value->getChildrens())>0){
                $separator .= '-';
                $return = $this->chi($value->getId(), $return, $separator);
            }
        }
        return $return;
    }
}