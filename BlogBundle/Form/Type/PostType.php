<?php

namespace BlogBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;

use Doctrine\ORM\EntityManager;

use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

use Symfony\Component\Validator\Constraints\All;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use A2lix\TranslationFormBundle\Form\Type\TranslationsType;

use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

class PostType extends AbstractType
{
    protected $em;
    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('translations', TranslationsType::class, array(
            'fields' => array(
                'author' => array(
                    'label' => 'Author',
                    'field_type' => TextType::class,
                    'required' => false
                ),
                'title' => array(
                    'label' => 'Title',
                    'field_type' => TextType::class,
                    'required' => true
                ),
                'brief' => array(
                    'label' => 'Brief',
                    'field_type' => TextareaType::class,
                    'required' => false
                ),
                'description' => array(
                    'label' => 'Post',
                    'field_type' => TextareaType::class,
                    'required' => false
                ),
                'slug' => array(
                    'label' => 'Alias',
                    'field_type' => TextType::class,
                    'required' => false
                ),
                'seo_title' => array(
                    'label' => 'Title',
                    'field_type' => TextType::class,
                    'required' => false
                ),
                'seo_keyword' => array(
                    'label' => 'Keyword',
                    'field_type' => TextType::class,
                    'required' => false
                ),
                'seo_description' => array(
                    'label' => 'Description',
                    'field_type' => TextareaType::class,
                    'required' => false
                ),
            ),
            'label' => false
        ));
        $builder->add('begin_date', DatetimeType::class, array('widget' => 'single_text', 'html5' => false));
        $builder->add('file', FileType::class, array('required' => false));
        $builder->add('files', FileType::class, array(
                'error_bubbling' => true,
                'required'=>false,
                'multiple'   => true,
                'data_class' => null,
                'constraints' => array(
                    new All(array(
                        'constraints' => array(
                            new File(array(
                                'maxSize'   => '20M',
                                'mimeTypes' => ["image/png","image/jpeg", "image/gif"],
                                ))
                            )
                    )))
                ));
        $builder->add('tag_id', ChoiceType::class, array(
            'placeholder' => '- необхідно обрати -',
            'choices' => $this->tree(),
        ));
        $builder->add('priority', EntityType::class, array(
            'class' => 'BlogBundle:Priority',
            'choice_label' => 'title',
            'placeholder' => '- необхідно обрати -'
        ));
        $builder->add('comment_access', EntityType::class, array(
            'class' => 'BlogBundle:Comment',
            'choice_label' => 'title',
            'placeholder' => '- необхідно обрати -'
        ));
        $builder->add('rss', EntityType::class, array(
            'class' => 'BlogBundle:Rss',
            'choice_label' => 'title',
            'placeholder' => '- необхідно обрати -'
        ));

        $builder
            ->add('similars', Select2EntityType::class, [
                'multiple' => true,
                'remote_route' => 'blog_post_search_similar',
                'class' => '\BlogBundle\Entity\Posts',
                'primary_key' => 'id',
                'text_property' => 'title',
                'minimum_input_length' => 2,
                'allow_clear' => true,
                'delay' => 250,
                'cache' => true,
                'cache_timeout' => 60000,
                'language' => 'en',
                'placeholder' => 'Select a similar news',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BlogBundle\Entity\Posts'
        ));
    }

    public function tree()
    {
        $parents = $this->em->getRepository("BlogBundle:Tags")->findAll();
        $return = [];
        foreach ($parents as $p) {
//            $children = [];
//            foreach($p->getChildrens() as $child){
//                $children[$child->getTitle()] = $child->getId();
//            }
            $return[$p->getTitle()] = $p->getId();

        }
        return $return;
    }
}