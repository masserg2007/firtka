<?php

namespace BlogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Tags
 *
 * @ORM\Entity
 * @ORM\Table(name="blog_tags")
 * @ORM\Entity(repositoryClass="BlogBundle\Entity\Repository\BlogRepository")
 */
class Tags
{
    use \A2lix\I18nDoctrineBundle\Doctrine\ORM\Util\Translatable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(name="pid", type="integer", nullable=true)
     */
    protected $pid;

    private $translations;

    /**
     * @ORM\OneToMany(targetEntity="Tags", mappedBy="parent")
     */
    private $childrens;

    /**
     * @ORM\ManyToOne(targetEntity="Tags", inversedBy="childrens")
     * @ORM\JoinColumn(name="pid", referencedColumnName="id")
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="Posts", mappedBy="tag", cascade={"ALL"}, orphanRemoval=true)
     **/
    private $posts;

    public function __construct()
    {
        $this->childrens = new ArrayCollection();
        $this->posts = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        $title = [];
        foreach ($this->translations as $translation)
        {
            $title[] = $translation->getTitle();
        }
        return reset($title);
    }

    public function getTitle()
    {
        if(!$this->getCurrentTranslation()){
            $title = 'No translations found...';
        } else {
            $title = $this->getCurrentTranslation()->getTitle();
        }
        return $title;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add children
     *
     * @param \BlogBundle\Entity\Tags $children
     *
     * @return Tag
     */
    public function addChildren(\BlogBundle\Entity\Tags $children)
    {
        $this->childrens[] = $children;

        return $this;
    }

    /**
     * Remove children
     *
     * @param \BlogBundle\Entity\Tags $children
     */
    public function removeChildren(\BlogBundle\Entity\Tags $children)
    {
        $this->childrens->removeElement($children);
    }

    /**
     * Get childrens
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildrens()
    {
        return $this->childrens;
    }

    /**
     * Set parent
     *
     * @param \BlogBundle\Entity\Tags $parent
     *
     * @return Tags
     */
    public function setParent(\BlogBundle\Entity\Tags $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \BlogBundle\Entity\Tags
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set pid
     *
     * @param integer $pid
     *
     * @return Tags
     */
    public function setPid($pid)
    {
        $this->pid = $pid;

        return $this;
    }

    /**
     * Get pid
     *
     * @return integer
     */
    public function getPid()
    {
        return $this->pid;
    }

    /**
     * Add post
     *
     * @param \BlogBundle\Entity\Posts $post
     *
     * @return Tags
     */
    public function addPost(\BlogBundle\Entity\Posts $post)
    {
        $this->posts[] = $post;

        return $this;
    }

    /**
     * Remove post
     *
     * @param \BlogBundle\Entity\Posts $post
     */
    public function removePost(\BlogBundle\Entity\Posts $post)
    {
        $this->posts->removeElement($post);
    }

    /**
     * Get posts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPosts()
    {
        return $this->posts;
    }
}
