<?php

namespace BlogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 *Blog
 *
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="blog_posts")
 * @ORM\Entity(repositoryClass="BlogBundle\Entity\Repository\PostsRepository")
 */
class Posts
{
    use \A2lix\I18nDoctrineBundle\Doctrine\ORM\Util\Translatable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", nullable=true)
     */
    private $path;

    /**
     * @ORM\Column(name="begin_date", type="datetime")
     */
    protected $begin_date;

    /**
     * @ORM\Column(name="creation", type="datetime")
     */
    protected $creation;

    /**
     * @ORM\Column(name="tag_id", type="integer")
     */
    protected $tag_id;

    /**
     * @ORM\Column(name="priority_id", type="integer")
     */
    protected $priority_id;

    /**
     * @ORM\Column(name="rss_id", type="integer")
     */
    protected $rss_id;

    /**
     * @ORM\Column(name="comment_access_id", type="integer")
     */
    protected $comment_access_id;

    /**
     * @ORM\Column(name="publicated", type="integer", nullable=true)
     */
    protected $publicated;

    private $translations;

    /**
     * @ORM\OneToMany(targetEntity="Comments", mappedBy="post", cascade={"ALL"}, orphanRemoval=true)
     **/
    private $comments;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Posts", mappedBy="similars", cascade={"persist"})
     */
    private $posts;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Posts", inversedBy="posts")
     * @ORM\JoinTable(name="blog_post_similar",
     *      joinColumns={@ORM\JoinColumn(name="parent_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="child_id", referencedColumnName="id")}
     * )
     */
    private $similars;

    /**
     * @ORM\ManyToOne(targetEntity="UsersBundle\Entity\Users", inversedBy="posts")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $users;

    /**
     * @ORM\ManyToOne(targetEntity="Tags", inversedBy="posts")
     * @ORM\JoinColumn(name="tag_id", referencedColumnName="id")
     */
    private $tag;

    /**
     * @ORM\ManyToOne(targetEntity="Priority", inversedBy="posts")
     * @ORM\JoinColumn(name="priority_id", referencedColumnName="id")
     */
    private $priority;

    /**
     * @ORM\ManyToOne(targetEntity="Comment", inversedBy="posts")
     * @ORM\JoinColumn(name="comment_access_id", referencedColumnName="id")
     */
    private $comment_access;

    /**
     * @ORM\ManyToOne(targetEntity="Rss", inversedBy="posts")
     * @ORM\JoinColumn(name="rss_id", referencedColumnName="id")
     */
    private $rss;

    /**
     * @ORM\OneToMany(targetEntity="BlogBundle\Entity\PostImages", mappedBy="post", cascade={"ALL"}, orphanRemoval=true)
     **/
    private $images;

    /**
     * @ORM\OneToMany(targetEntity="BlogBundle\Entity\PostViews", mappedBy="post", cascade={"ALL"}, orphanRemoval=true)
     **/
    private $views;

    /**
     * @var array
     */
    protected $files;

    /**
     * Sets files.
     * @param array $files
     * @return Auto
     */
    public function setFiles($files)
    {
        $this->files = $files;

        return $this;
    }

    /**
     * Get file.
     * @return array
     */
    public function getFiles()
    {
        return $this->files;
    }

    public function __construct()
    {
        $this->comments = new ArrayCollection();
        $this->images = new ArrayCollection();
        $this->views = new ArrayCollection();

        $this->similars = new ArrayCollection();
        $this->posts = new ArrayCollection();
    }

    /**
     * @Assert\File(
     *   maxSize="10M",
     *   mimeTypesMessage = "Дозволені файли тільки у форматі: png, jpg, jpeg, gif !!!",
     *   mimeTypes = {
     *          "image/png",
     *          "image/jpeg",
     *          "image/jpg",
     *          "image/gif"
     *          }
     *  )
     */
    protected $file;

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->getFile()) {
            if(null !== $this->getPath()){
                if(is_file($this->getUploadRootDir().'/'.$this->getPath())){
                    unlink($this->getUploadRootDir().'/'.$this->getPath());
                }
            }
            $this->path = uniqid().'.'.$this->getFile()->guessExtension();
            $name = $this->generateRandomFilename();
            $this->path = $name.'.'.$this->getFile()->guessExtension();
        }
    }

    /**
     * Generates a 32 char long random filename
     *
     * @return string
     */
    public function generateRandomFilename()
    {
        $count = 0;
        do {
            $random = random_bytes(16);
            $randomString = bin2hex($random);
            $count++;
        }
        while(file_exists($this->getUploadRootDir().'/'.$randomString.'.'.$this->getFile()->guessExtension()) && $count < 50);
        return $randomString;
    }

    public function getAbsolutePath() { return null === $this->path ? null : $this->getUploadRootDir().'/'.$this->path; }
    public function getWebPath() { return null === $this->path ? null : $this->getUploadDir().'/'.$this->path; }
    protected function getUploadRootDir() { return __DIR__.'/../../../web/'.$this->getUploadDir(); }
    protected function getUploadDir() { return 'data/blog/'.$this->id; }

    /**
     * Sets file.
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    /**
     * Get file.
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->getFile()) {
            return;
        }
        $this->getFile()->move($this->getUploadRootDir().'/', $this->path);

        $this->file = null;
    }

    /**
     * @return integer
     */
    protected $deletedId;

    /**
     * @ORM\PreRemove()
     */
    public function deletedFilePath()
    {
        $this->deletedId = $this->id;
        return  $this->deletedId;
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        $file = __DIR__.'/../../../web/data/blog/'.$this->deletedId.'/'.$this->path;
        if (is_file($file)) {
            unlink($file);
        }
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        $title = [];
        foreach ($this->translations as $translation)
        {
            $title[] = $translation->getTitle();
        }

        return reset($title);
    }

    public function getTitle()
    {
        if(!$this->getCurrentTranslation()){
            $title = 'No translations found...';
        } else {
            $title = $this->getCurrentTranslation()->getTitle();
        }
        return $title;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set path
     *
     * @param string $path
     *
     * @return Posts
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set creation
     *
     * @param \DateTime $creation
     *
     * @return Posts
     */
    public function setCreation($creation)
    {
        $this->creation = $creation;

        return $this;
    }

    /**
     * Get creation
     *
     * @return \DateTime
     */
    public function getCreation()
    {
        return $this->creation;
    }

    /**
     * Add comment
     *
     * @param \BlogBundle\Entity\Comments $comment
     *
     * @return Posts
     */
    public function addComment(\BlogBundle\Entity\Comments $comment)
    {
        $this->comments[] = $comment;

        return $this;
    }

    /**
     * Remove comment
     *
     * @param \BlogBundle\Entity\Comments $comment
     */
    public function removeComment(\BlogBundle\Entity\Comments $comment)
    {
        $this->comments->removeElement($comment);
    }

    /**
     * Get comments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set beginDate
     *
     * @param \DateTime $beginDate
     *
     * @return Posts
     */
    public function setBeginDate($beginDate)
    {
        $this->begin_date = $beginDate;

        return $this;
    }

    /**
     * Get beginDate
     *
     * @return \DateTime
     */
    public function getBeginDate()
    {
        return $this->begin_date;
    }

    /**
     * Set post
     *
     * @param \BlogBundle\Entity\Posts $post
     *
     * @return Posts
     */
    public function setPost(\BlogBundle\Entity\Posts $post = null)
    {
        $this->post = $post;

        return $this;
    }

    /**
     * Get post
     *
     * @return \BlogBundle\Entity\Posts
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * Set priorities
     *
     * @param \BlogBundle\Entity\Priority $priorities
     *
     * @return Posts
     */
    public function setPriorities(\BlogBundle\Entity\Priority $priorities = null)
    {
        $this->priorities = $priorities;

        return $this;
    }

    /**
     * Set rss
     *
     * @param \BlogBundle\Entity\Rss $rss
     *
     * @return Posts
     */
    public function setRss(\BlogBundle\Entity\Rss $rss = null)
    {
        $this->rss = $rss;

        return $this;
    }

    /**
     * Get rss
     *
     * @return \BlogBundle\Entity\Rss
     */
    public function getRss()
    {
        return $this->rss;
    }

    /**
     * Add image
     *
     * @param \BlogBundle\Entity\PostImages $image
     *
     * @return Posts
     */
    public function addImage(\BlogBundle\Entity\PostImages $image)
    {
        $this->images[] = $image;
        $image->setPost($this);
        return $this;
    }

    /**
     * Remove image
     *
     * @param \BlogBundle\Entity\PostImages $image
     */
    public function removeImage(\BlogBundle\Entity\PostImages $image)
    {
        $this->images->removeElement($image);
    }

    /**
     * Get images
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Set priority
     *
     * @param \BlogBundle\Entity\Priority $priority
     *
     * @return Posts
     */
    public function setPriority(\BlogBundle\Entity\Priority $priority = null)
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * Get priority
     *
     * @return \BlogBundle\Entity\Priority
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * Set tag
     *
     * @param \BlogBundle\Entity\Tags $tag
     *
     * @return Posts
     */
    public function setTag(\BlogBundle\Entity\Tags $tag = null)
    {
        $this->tag = $tag;

        return $this;
    }

    /**
     * Get tag
     *
     * @return \BlogBundle\Entity\Tags
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * Set tagId
     *
     * @param integer $tagId
     *
     * @return Posts
     */
    public function setTagId($tagId)
    {
        $this->tag_id = $tagId;

        return $this;
    }

    /**
     * Get tagId
     *
     * @return integer
     */
    public function getTagId()
    {
        return $this->tag_id;
    }

    /**
     * Set priorityId
     *
     * @param integer $priorityId
     *
     * @return Posts
     */
    public function setPriorityId($priorityId)
    {
        $this->priority_id = $priorityId;

        return $this;
    }

    /**
     * Get priorityId
     *
     * @return integer
     */
    public function getPriorityId()
    {
        return $this->priority_id;
    }

    /**
     * Set rssId
     *
     * @param integer $rssId
     *
     * @return Posts
     */
    public function setRssId($rssId)
    {
        $this->rss_id = $rssId;

        return $this;
    }

    /**
     * Get rssId
     *
     * @return integer
     */
    public function getRssId()
    {
        return $this->rss_id;
    }

    /**
     * Set commentId
     *
     * @param integer $commentId
     *
     * @return Posts
     */
    public function setCommentId($commentId)
    {
        $this->comment_id = $commentId;

        return $this;
    }

    /**
     * Get commentId
     *
     * @return integer
     */
    public function getCommentId()
    {
        return $this->comment_id;
    }

    /**
     * Set commentAccessId
     *
     * @param integer $commentAccessId
     *
     * @return Posts
     */
    public function setCommentAccessId($commentAccessId)
    {
        $this->comment_access_id = $commentAccessId;

        return $this;
    }

    /**
     * Get commentAccessId
     *
     * @return integer
     */
    public function getCommentAccessId()
    {
        return $this->comment_access_id;
    }

    /**
     * Set commentAccess
     *
     * @param \BlogBundle\Entity\Comment $commentAccess
     *
     * @return Posts
     */
    public function setCommentAccess(\BlogBundle\Entity\Comment $commentAccess = null)
    {
        $this->comment_access = $commentAccess;

        return $this;
    }

    /**
     * Get commentAccess
     *
     * @return \BlogBundle\Entity\Comment
     */
    public function getCommentAccess()
    {
        return $this->comment_access;
    }

    /**
     * Set users
     *
     * @param \UsersBundle\Entity\Users $users
     *
     * @return Posts
     */
    public function setUsers(\UsersBundle\Entity\Users $users = null)
    {
        $this->users = $users;

        return $this;
    }

    /**
     * Get users
     *
     * @return \UsersBundle\Entity\Users
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Add view
     *
     * @param \BlogBundle\Entity\PostViews $view
     *
     * @return Posts
     */
    public function addView(\BlogBundle\Entity\PostViews $view)
    {
        $this->views[] = $view;
        $view->setPost($this);
        return $this;
    }

    /**
     * Remove view
     *
     * @param \BlogBundle\Entity\PostViews $view
     */
    public function removeView(\BlogBundle\Entity\PostViews $view)
    {
        $this->views->removeElement($view);
    }

    /**
     * Get views
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getViews()
    {
        return $this->views;
    }

    /**
     * Set publicated
     *
     * @param integer $publicated
     *
     * @return Posts
     */
    public function setPublicated($publicated)
    {
        $this->publicated = $publicated;

        return $this;
    }

    /**
     * Get publicated
     *
     * @return integer
     */
    public function getPublicated()
    {
        return $this->publicated;
    }

    public function getDateCreation()
    {
        return $this->creation->format('Y-m-d');
    }

    /**
     * Add similar
     *
     * @param \BlogBundle\Entity\Posts $similar
     *
     * @return Posts
     */
    public function addSimilar(\BlogBundle\Entity\Posts $similar)
    {
        $this->similars[] = $similar;
        $similar->setPost($this);
        return $this;
    }

    /**
     * Remove similar
     *
     * @param \BlogBundle\Entity\Posts $similar
     */
    public function removeSimilar(\BlogBundle\Entity\Posts $similar)
    {
        $this->similars->removeElement($similar);
    }

    /**
     * Get similars
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSimilars()
    {
        return $this->similars;
    }

    /**
     * Add post
     *
     * @param \BlogBundle\Entity\Posts $post
     *
     * @return Posts
     */
    public function addPost(\BlogBundle\Entity\Posts $post)
    {
        $this->posts[] = $post;

        return $this;
    }

    /**
     * Remove post
     *
     * @param \BlogBundle\Entity\Posts $post
     */
    public function removePost(\BlogBundle\Entity\Posts $post)
    {
        $this->posts->removeElement($post);
    }

    /**
     * Get posts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPosts()
    {
        return $this->posts;
    }
}
