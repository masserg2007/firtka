<?php

namespace BlogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * BlogBundle\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="blog_images")
 */

class PostImages
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="post_id", type="integer")
     */
    protected $post_id;

    /**
     * @ORM\Column(name="title", type="string")
     */
    protected $title;

    /**
     * @ORM\Column(name="path", type="string")
     */
    protected $path;

    /**
     * @ORM\Column(name="img_type", type="string")
     */
    protected $img_type;

    /**
     * @ORM\Column(name="img_size", type="string")
     */
    protected $img_size;

    /**
     * @ORM\Column(name="creation", type="datetime")
     */
    protected $creation;

    /**
     * @ORM\ManyToOne(targetEntity="BlogBundle\Entity\Posts", inversedBy="images")
     * @ORM\JoinColumn(name="post_id", referencedColumnName="id")
     */
    private $post;

    /**
     * @Assert\File(
     *   maxSize="10M",
     *   mimeTypesMessage = "Дозволені файли тільки у форматі: png, jpg, jpeg, gif !!!",
     *   mimeTypes = {
     *          "image/png",
     *          "image/jpeg",
     *          "image/jpg",
     *          "image/gif"
     *          }
     *  )
     */
    protected $file;

    /**
     * Sets file.
     */
    public function setFile($file)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file.
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @var array
     */
    protected $files;

    /**
     * Sets files.
     * @param array $files
     * @return Auto
     */
    public function setFiles($files)
    {
        $this->files = $files;

        return $this;
    }

    /**
     * Get file.
     * @return array
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set postId
     *
     * @param integer $postId
     *
     * @return PostImages
     */
    public function setPostId($postId)
    {
        $this->post_id = $postId;

        return $this;
    }

    /**
     * Get postId
     *
     * @return integer
     */
    public function getPostId()
    {
        return $this->post_id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return PostImages
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set path
     *
     * @param string $path
     *
     * @return PostImages
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set imgType
     *
     * @param string $imgType
     *
     * @return PostImages
     */
    public function setImgType($imgType)
    {
        $this->img_type = $imgType;

        return $this;
    }

    /**
     * Get imgType
     *
     * @return string
     */
    public function getImgType()
    {
        return $this->img_type;
    }

    /**
     * Set imgSize
     *
     * @param string $imgSize
     *
     * @return PostImages
     */
    public function setImgSize($imgSize)
    {
        $this->img_size = $imgSize;

        return $this;
    }

    /**
     * Get imgSize
     *
     * @return string
     */
    public function getImgSize()
    {
        return $this->img_size;
    }

    /**
     * Set creation
     *
     * @param \DateTime $creation
     *
     * @return PostImages
     */
    public function setCreation($creation)
    {
        $this->creation = $creation;

        return $this;
    }

    /**
     * Get creation
     *
     * @return \DateTime
     */
    public function getCreation()
    {
        return $this->creation;
    }

    /**
     * Set post
     *
     * @param \BlogBundle\Entity\Posts $post
     *
     * @return PostImages
     */
    public function setPost(\BlogBundle\Entity\Posts $post = null)
    {
        $this->post = $post;

        return $this;
    }

    /**
     * Get post
     *
     * @return \BlogBundle\Entity\Posts
     */
    public function getPost()
    {
        return $this->post;
    }
}
