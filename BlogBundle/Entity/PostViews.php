<?php

namespace BlogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BlogBundle\Entity
 *
 * @ORM\Entity(repositoryClass="BlogBundle\Entity\Repository\ViewsRepository")
 * @ORM\Table(name="blog_post_views")
 */

class PostViews
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="user_id", type="integer", nullable=true)
     */
    protected $user_id;

    /**
     * @ORM\Column(name="user_ip", type="string")
     */
    protected $user_ip;

    /**
     * @ORM\Column(name="post_id", type="integer")
     */
    protected $post_id;

    /**
     * @ORM\Column(name="creation", type="datetime")
     */
    protected $creation;

    /**
     * @ORM\ManyToOne(targetEntity="BlogBundle\Entity\Posts", inversedBy="views")
     * @ORM\JoinColumn(name="post_id", referencedColumnName="id")
     */
    private $post;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return PostViews
     */
    public function setUserId($userId)
    {
        $this->user_id = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set userIp
     *
     * @param string $userIp
     *
     * @return PostViews
     */
    public function setUserIp($userIp)
    {
        $this->user_ip = $userIp;

        return $this;
    }

    /**
     * Get userIp
     *
     * @return string
     */
    public function getUserIp()
    {
        return $this->user_ip;
    }

    /**
     * Set postId
     *
     * @param integer $postId
     *
     * @return PostViews
     */
    public function setPostId($postId)
    {
        $this->post_id = $postId;

        return $this;
    }

    /**
     * Get postId
     *
     * @return integer
     */
    public function getPostId()
    {
        return $this->post_id;
    }

    /**
     * Set creation
     *
     * @param \DateTime $creation
     *
     * @return PostViews
     */
    public function setCreation($creation)
    {
        $this->creation = $creation;

        return $this;
    }

    /**
     * Get creation
     *
     * @return \DateTime
     */
    public function getCreation()
    {
        return $this->creation;
    }

    /**
     * Set post
     *
     * @param \BlogBundle\Entity\Posts $post
     *
     * @return PostViews
     */
    public function setPost(\BlogBundle\Entity\Posts $post = null)
    {
        $this->post = $post;

        return $this;
    }

    /**
     * Get post
     *
     * @return \BlogBundle\Entity\Posts
     */
    public function getPost()
    {
        return $this->post;
    }
}
